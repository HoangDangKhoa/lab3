<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Danh sách sản phẩm</title>
</head>
<body>
    <?php 
        // var_dump($san_pham);
    ?>
    <table border="1">
        <caption>Danh sách các sản phẩm</caption>
        <thead>
            <tr>
                <td>STT</td>
                <td>Tên sản phẩm</td>
                <td>Tên hiển thị</td>
                <td>Sửa</td>
                <td>Xóa</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($san_pham as $value) { ?>
                <tr>
                    <td>{{ $value['id'] }}</td>
                    <td>{{ $value['name'] }}</td>
                    <td>{{ $value['display_name'] }}</td>
                    <td><a href="sua/{{$value['id']}}">Sửa</a></td>
                    <td><a href="xoa/{{$value['id']}}">Xóa</a></td>
                </tr> 
            <?php } ?>

        </tbody>
    </table>
    <a href="product/them">Thêm sản phẩm</a>
</body>
</html>