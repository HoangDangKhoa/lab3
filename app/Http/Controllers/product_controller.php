<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\product;

class product_controller extends Controller
{
    public function hienthi() {
        $san_pham = product::all()->toArray();
        return view('product/hienthi')->with('san_pham',$san_pham);
    }

    public function them() {
        return view('product/them');
    }
    public function save(Request $rq) {
        $sp = new product();
        $sp->username = $rq->username;
        $sp->display_name = $rq->display_name;
        $sp->save();
        return redirect()->Route('index');
    }
}
